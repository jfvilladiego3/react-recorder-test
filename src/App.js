import React from 'react';
import logo from './logo.svg';
import './App.css';

import VideoRecorder from 'react-video-recorder'

function App() {
  return (
    <div className="App">
      <VideoRecorder 
      onRecordingComplete={(videoBlob) => {
        // Do something with the video...
        console.log('videoBlob', videoBlob)
      }} 
    />
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
